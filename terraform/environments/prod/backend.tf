terraform {
    backend "gcs" {
        bucket = "product-tfstate-bucket"
        prefix = "env/prod"
    }
}