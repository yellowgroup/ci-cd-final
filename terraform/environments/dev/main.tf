provider "google" {
  project = var.project_id
}

data "google_cloud_run_locations" "default" {
 }
resource "google_cloud_run_service" "default" {

  name     = "cr-${var.region}-${var.branch}"
  location = var.region
  project  = var.project_id
  template {
    spec {
      containers {
          image = var.image 

          ports {
            container_port = 8000
          }
      }
    }
  }
}
data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}
resource "google_cloud_run_service_iam_policy" "noauth" {

  location    = google_cloud_run_service.default.location
  project     = google_cloud_run_service.default.project
  service     = google_cloud_run_service.default.name

  policy_data = data.google_iam_policy.noauth.policy_data
}
resource "google_compute_region_network_endpoint_group" "default" {

  name                  = "cr-region1-neg-${var.branch}"
  network_endpoint_type = "SERVERLESS"
  region                = google_cloud_run_service.default.location
  cloud_run {
    service = google_cloud_run_service.default.name
  }
}
module "lb-http" {
  source            = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
  version           = "~> 4.5"

  project           = var.project_id
  name              = "cr-lb-http-${var.branch}"

  ssl                             = false
  managed_ssl_certificate_domains = []
  https_redirect                  = false
  backends = {
    default = {
      description            = null
      enable_cdn             = false
      custom_request_headers = null

      log_config = {
        enable      = true
        sample_rate = 1.0
      }

      groups = [
        {
          group = google_compute_region_network_endpoint_group.default.id
        }

      ]

      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }
      security_policy = null
    }
  }
}

