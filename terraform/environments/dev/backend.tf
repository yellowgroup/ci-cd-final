terraform {
    backend "gcs" {
        bucket = "dev-tfstate-bucket"
        prefix = "env/dev"
    }
}