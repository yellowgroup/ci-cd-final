variable "region" {
    description = "region to deploy cloud run into"
    default     = "us-central1"
    
}

variable "image" {
    type        = string
    description = "docker image to be attached into cloud run instances"

}

variable "project_id" {
    type        = string
    description = "GCP Project ID"
}

variable "branch" {
    type        = string
    description = "repo branch"
}

